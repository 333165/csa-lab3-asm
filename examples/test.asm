section data:
    input: 0
    output: 1
section text:
    _start:
        lw r1, input
        lw r2, output
        loop:
            lw r3, r1
            sw r2, r3
            beq r3, r0, end
            jmp loop
        end:
            halt